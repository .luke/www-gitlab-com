---
layout: markdown_page
title: 'IDC Innovators Agile Code Development Technologies, 2018'
---

Innovative DevOps teams are embracing the latest technologies to accelerate their development practices. It's no surprise that focusing on emerging vendors in the agile code development market is a priority to dramatically streamline their update process.

![IDC Innovators logo graphic png](/images/logos/idc-innovator.png){: .margin-bottom20 .margin-top20}

International Data Corporation published an IDC Innovators report on the agile code development space to help IT professionals, business executives, and the investment community make fact-based decisions on their DevOps technology investments.

Download the full report to learn why GitLab was chosen as an IDC Innovator, including an overall assessment, key differentiators, challenges in the market, and related research.
