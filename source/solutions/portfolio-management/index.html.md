---
layout: markdown_page
title: "Portfolio Management"
---
## Agile Portfolio Management
As multiple projects scale, with parallel value streams and efforts, the organization needs to adopt processes to manage and govern the portfolio of agile projects, both in flight and proposed.  

## Plan future work

- Organize new business intiatives and efforts into [epics](https://docs.gitlab.com/ee/user/group/epics/), where you can plan specific requirements, tasks and work in GitLab Issues and Merge Requests
- Prioritize and visualize the sequence of delivery
- Share, collaborate and iterate on the details of portfolio plans.
- Visualize [roadmaps](https://docs.gitlab.com/ee/user/group/roadmap/) to communicate timing and sequencing of delivering buisness value.

